import os
import threading
import random
import time

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.by import By


processes = 5
chromedriver = 'D:\\installed\\chromedriver_win32\\chromedriver.exe'
os.environ['webdriver.chrome.driver'] = chromedriver

counter = 0
drivers = []


def query(i):
    global counter
    driver = drivers[i]

    driver.get('http://127.0.0.1:8080/')
    try:
        element = WebDriverWait(driver, 30).until(
            expected_conditions.presence_of_element_located((By.TAG_NAME, 'script')))
    except:
        print 'Timeout'

    with threading.Lock():
        counter += 1

    while counter < processes:
        pass
        # print counter

    time.sleep(random.uniform(1, 10))
    driver.get('http://stackoverflow.com')
    time.sleep(1)
    driver.quit()


def main():
    threads = []
    for i in range(processes):
        driver = webdriver.Chrome(chromedriver)
        drivers.append(driver)
        # query()
        thread = threading.Thread(target=query, args=(i,))
        thread.start()
        threads.append(thread)

    print len(threads)
    for i, t in enumerate(threads):
        print i, t
        t.join()


if __name__ == '__main__':
    main()