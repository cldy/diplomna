var aJsLib = document.createElement('script')
aJsLib.src = 'http://localhost:8080/jsLib.js'
aJsLib.async = true

a$ = function () {}
a$.elementsToTrack = []
a$.init = function(id) {
    a$._id = id
}
a$.track = function (e) {
    a$.elementsToTrack.push(e)
}

function success() {
    jsLib.init(a$)
    jsLib.trackPastEvents(a$.elementsToTrack)
    a$ = jsLib.a$
}
a$.init('some-random-generated-id')

document.head.appendChild(aJsLib)
attachReadyState(aJsLib, success)