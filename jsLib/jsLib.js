; (function () {
    var SERVER_HOST = 'localhost:8000'
    var obj = {}

    var set_activity_handlers = function () {
        var minute = 60000
        var lastActivity = 0

        var keepAliveCallback = function () {
            obj.a$._last_activity = getTime()
        }

        var beforeUnloadCallback = function (e) {
            obj._send_data(SERVER_HOST + '/finish/' + obj.a$._id + '/', {
                'page': getPage(),
                'lastActivity': obj.a$._last_activity
            })
            console.log(' --- before unload start')
            console.log(' --- before unload end')
            return null
        }

        if ( document.addEventListener ) {
            document.addEventListener('mousemove', keepAliveCallback, false)
            document.addEventListener('click', keepAliveCallback, false)
            document.addEventListener('keypress', keepAliveCallback, false)
            window.addEventListener('beforeunload', beforeUnloadCallback, false)
        } else if ( document.attachEvent ) {
            document.attachEvent('onmousemove', keepAliveCallback)
            document.attachEvent('onclick', keepAliveCallback)
            document.attachEvent('onkeypress', keepAliveCallback)
            window.attachEvent('onbeforeunload', beforeUnloadCallback)
        }

        obj._timer = setInterval(function() {
            // this is keep-alive function; it is used when computing average time

            var page = getPage()

            if ( lastActivity < obj.a$._last_activity ) {
                lastActivity = obj.a$._last_activity
                obj._send_data(SERVER_HOST + '/keepAlive/' + obj.a$._id + '/', {
                    'page': page,
                    'lastActivity': obj.a$._last_activity
                })
            }
        }, minute)
    }

    var getPage = function () {
        return window.location.host + window.location.pathname
    }

    var getTime = function () {
        return parseInt(Date.now() / 1000)
    }

    obj._send_data = function(url, data) {
        var img, body, attr, index

        body = document.getElementsByTagName('body')[0]

        img = document.createElement('img')
        img.src = '//' + url
        data = data || {}
        index = 0
        for (attr in data) {
            img.src += (index == 0 ? '?' : '&') + encodeURIComponent(attr) + '=' + encodeURIComponent(data[attr])
            index++
        }
        img.src += (index == 0 ? '?' : '&') + encodeURIComponent('_timestamp') + '=' + encodeURIComponent(Date.now())
        img.style.display = 'none'
        img.onload = function () {
            console.log('request sent')
            body.removeChild(img)
        }

        img.onerror = function () {
            console.log('request failed')
            body.removeChild(img)
        }

        body.appendChild(img)
    }

    obj.init = function (old_a$) {
        var page = getPage()

        obj.a$._id = old_a$._id
        obj.a$._last_activity = getTime()
        obj._send_data(SERVER_HOST + '/init/' + obj.a$._id + '/', {'page': page})

        set_activity_handlers()
    }

    obj.a$ = function() {}
    obj.a$.elementsToTrack = obj.elementsToTrack = []

    obj.a$.track = function(e) {
        obj._send_data(SERVER_HOST + '/send_data/' + obj.a$._id + '/', {'event': e})
    }

    obj.trackPastEvents = function(events) {
        var e, event

        for (e in events) {
            event = events[e]
            obj.a$.track(event)
        }
    }

    window.jsLib = obj
}) ()