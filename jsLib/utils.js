attachReadyState = function (el, callback) {
    var done = false
    el.onreadystatechange = el.onload = function () {
        if (!done && (!this.readyState ||
            this.readyState == 'loaded' || this.readyState == 'complete')) {
            done = true
            callback()

            el.onload = el.onreadystatechange = null
            document.head.removeChild(el)
        }
    }
}