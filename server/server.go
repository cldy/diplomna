package main

import (
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"github.com/xuyu/goredis"
	"math/rand"
	"net/http"
	"strconv"
	"strings"
	"time"
)

const (
	host            = "localhost:6379"
	protocol        = "tcp"
	password        = ""
	db              = 1
	maxidle         = 10
	timeout         = 30 * time.Second
	persistentKey	= "persistentkey"
//	managerpassword = "supersecretpassword"
)

var (
	client *goredis.Redis
)

func log(a ...interface {}) {
	str := fmt.Sprint(time.Now())
	str = "[" + strings.Split(str, ".")[0] + "] "
	fmt.Print(str)
	fmt.Println(a...)
}

func checkRequest(r *http.Request) (analyticsId string, ok bool) {
	args := r.URL.Query()
	ok = true
	analyticsId = ""

	if _, ok = args["_timestamp"]; !ok {
		log(" --> drop request; no \"_timestamp\" found")
		return
	}

	components := strings.Split(r.URL.Path, "/")
	if len(components) < 3 {
		log(" --> drop request; invalid path")
		ok = false
		return
	}

	analyticsId = components[2]
	log(" --> request OK")
	return
}

func generateRandomId(length int) string {
	var alpha = "abcdefghijklmnopqrstuvwxyz0123456789"
	var alphaLen = len(alpha)
	var res []byte = make([]byte, length)

	for i := 0; i < length; i++ {
		res[i] = alpha[rand.Intn(alphaLen)]
	}

	return string(res)
}

func manageCookies(w *http.ResponseWriter, r *http.Request) (cookieId string) {
	var cookie, err = r.Cookie("init")

	log(" --> manage cookies")

	if err == nil {
		log("  --- cookie value: ", cookie.Value)
		cookieId = cookie.Value
	} else {
		log("  --- set cookie")
		cookieId = generateRandomId(32)

		http.SetCookie(*w, &http.Cookie{
			Name:   "init",
			Value:  cookieId,
			Path:   "/",
			MaxAge: 2592000, // 30 days
		})
	}

	return
}

func returnImage(w *http.ResponseWriter) {
	pixelData := []byte{
		137, 80, 78, 71, 13, 10, 26, 10, 0, 0, 0, 13, 73, 72, 68, 82, 0, 0, 0,
		1, 0, 0, 0, 1, 8, 2, 0, 0, 0, 144, 119, 83, 222, 0, 0, 0, 1, 115, 82, 71,
		66, 0, 174, 206, 28, 233, 0, 0, 0, 4, 103, 65, 77, 65, 0, 0, 177, 143, 11,
		252, 97, 5, 0, 0, 0, 9, 112, 72, 89, 115, 0, 0, 14, 195, 0, 0, 14, 195, 1,
		199, 111, 168, 100, 0, 0, 0, 12, 73, 68, 65, 84, 24, 87, 99, 248, 255, 255,
		63, 0, 5, 254, 2, 254, 167, 53, 129, 132, 0, 0, 0, 0, 73, 69, 78, 68, 174,
		66, 96, 130,
	}
	(*w).Header().Set("Content-type", "image/png")
	(*w).Write(pixelData)
}

func trackHandler(w http.ResponseWriter, r *http.Request) {
	var (
		analyticsId string
		ok          bool
	)

	log("\n-- track: ", r.URL.Path, " ; args: ", r.URL.Query())
	if analyticsId, ok = checkRequest(r); !ok {
		return
	}

	cookieId := manageCookies(&w, r)
	event := getArg(r, "event")

	if event == "" {
		log(" -- bad request; no event data")
		return
	}

	listId := formatTrackList(analyticsId, event)

	client.RPush(listId, cookieId+":"+timestampStr())

	returnImage(&w)
}

//func clean(analyticsId, event string, endTimestamp int64) {
//	var (
//		timestamp int64
//		item      string
//		bitem     []byte
//	)
//
//	listId := formatTrackList(analyticsId, event)
//
//	for {
//		bitem, _ = client.LIndex(listId, 0)
//		if bitem == nil {
//			break
//		}
//
//		item = string(bitem)
//		log("   --> ts: ", item)
//
//		data := strings.Split(item, ":")
//		timestamp, _ = strconv.ParseInt(data[1], 10, 64)
//		if timestamp <= endTimestamp {
//			client.LPop(listId)
//		} else {
//			break
//		}
//	}
//}
//
//func cleanHandler(w http.ResponseWriter, r *http.Request) {
//	var (
//		analyticsId   = r.FormValue("analyticsId")
//		password      = r.FormValue("password")
//		event         = r.FormValue("event")
//		sendTimestamp = r.FormValue("endTimestamp")
//	)
//	log(" -- in clean: ")
//
//	if password != managerpassword {
//		w.Write([]byte("fail"))
//		return
//	}
//
//	endTimestamp, _ := strconv.ParseInt(sendTimestamp, 10, 64)
//	clean(analyticsId, event, endTimestamp)
//
//	log(" -- exit clean")
//	w.Write([]byte("ok"))
//}

func formatTrackList(analyticsId string, event string) string {
	return analyticsId + ":track:" + hashPage(event)
}

func periodInt() int64 {
	now := time.Now()

	res, _ := strconv.ParseInt(fmt.Sprintf("%d%02d%02d", now.Year(), now.Month(), now.Day()), 10, 64)
	return res
}

func periodFloat() float64 {
	period := periodInt()
	return float64(period)
}

func getArg(r *http.Request, arg string) string {
	temp := r.URL.Query()[arg]
	if len(temp) != 1 {
		return ""
	}
	return temp[0]
}

func addTimeCheck(analyticsId, cookieId, page string) {
	formatStr := formatAverageTimePageSession(analyticsId, page, cookieId)
	client.Set(formatStr, timestampStr(), 0, 0, false, false)
	client.Expire(formatStr, 1800)
}

func formatRequest(analyticsId string, query string) string {
	req := hashPage(query)
	return analyticsId + ":req:" + req
}

func checkDropRequest(analyticsId string, query string) bool {
	req := formatRequest(analyticsId, query)

	if val, _ := client.Get(req); val != nil {
		log("  -- drop request due to expire (same requests between short period)")
		return true
	}

	client.Set(req, "1", 0, 0, false, false)
	client.Expire(req, 1)

	return false
}

func initHandler(w http.ResponseWriter, r *http.Request) {
	var (
		page        string
		analyticsId string
		ok          bool
	)

	log("\n-- init: ", r.URL.Path, " ; args: ", r.URL.Query())

	if analyticsId, ok = checkRequest(r); !ok {
		return
	}
	if checkDropRequest(analyticsId, r.URL.RawQuery) {
		return
	}

	page = getArg(r, "page")
	if page == "" {
		log("   -->  drop request; no \"page\" argument in init")
		return
	}

	cookieId := manageCookies(&w, r)

	addPageLoads(analyticsId, page)
	addVisitors(analyticsId, cookieId)
	addUniquePageLoads(analyticsId, cookieId, page)
	addTimeCheck(analyticsId, cookieId, page)

	returnImage(&w)
}

func formatVisitors(analyticsId string) string {
	return analyticsId + ":visitors"
}

func formatVisitor(analyticsId, cookieId string) string {
	return analyticsId + ":" + cookieId + ":visitor"
}

func addVisitors(analyticsId, cookieId string) {
	client.SAdd(formatVisitors(analyticsId), cookieId)
	client.Set(formatVisitor(analyticsId, cookieId), "1", 0, 0, false, false)
	client.Expire(formatVisitor(analyticsId, cookieId), 1800)
}

func usersHandler(w http.ResponseWriter, r *http.Request) {
	resp := 0
	analyticsId := r.FormValue("analyticsId")
	cookies, _ := client.SMembers(formatVisitors(analyticsId))
	for i := 0; i < len(cookies); i++ {
		val, _ := client.Get(formatVisitor(analyticsId, cookies[i]))
		if val != nil {
			resp++
		} else {
			client.SRem(formatVisitors(analyticsId), cookies[i])
		}
	}

	message, _ := json.MarshalIndent(resp, "", " ")
	w.Header().Set("Content-type", "application/json")
	w.Write(message)
}

func hashPage(page string) string {
	var byte_arr = md5.Sum([]byte(page))
	return hex.EncodeToString(byte_arr[:])
}

func formatPageLoads(analyticsId, page string) string {
	hash := hashPage(page)
	period := periodInt()

	return fmt.Sprintf("%s:%d:%s", analyticsId, period, hash)
}

func formatUniquePageLoadsTimes(analyticsId, page string) string {
	return fmt.Sprintf("%s:%s:times", analyticsId, hashPage(page))
}

func formatUniquePageLoads(analyticsId, page, period string) string {
	return fmt.Sprintf("un:%s:%s:%s", analyticsId, hashPage(page), period)
}

func addUniquePageLoads(analyticsId, cookieId, page string) {
	t := fmt.Sprintf("%d", periodInt())

	client.SAdd(formatUniquePageLoadsTimes(analyticsId, page), t)
	client.SAdd(formatUniquePageLoads(analyticsId, page, t), cookieId)
}

func formatPages(analyticsId string) string {
	return analyticsId + ":pages"
}

func formatPage(analyticsId, page string) string {
	return analyticsId + ":" + hashPage(page)
}

func addPageLoads(analyticsId, page string) {
	client.SAdd(formatPages(analyticsId), page)
	client.HIncrBy(formatPage(analyticsId, page), fmt.Sprintf("%d", periodInt()), 1)
}

func funnelHandler(w http.ResponseWriter, r *http.Request) {
	var (
		analyticsId = r.FormValue("analyticsId")
		f           []string
		M           map[string]int64 = make(map[string]int64)
		M2			map[string]int64
		i           int
		j           int
		le          int
		ll          int64
		listId      string
		tmp         []string
		cookieId    string
		stimestamp  string
		timestamp   int64
		bdata       []byte
		data        string
		res         []int
		ok			bool
		ok2			bool
		val			int64
		d1			string
		d2			string
		start		int
		end			int
	)

	log("\n-- funnel: ", r.URL.Path, " ; args: ", r.URL.Query())
	log(r.FormValue("funnel"))
	d1 = r.FormValue("d1")
	d2 = r.FormValue("d2")

	json.Unmarshal([]byte(r.FormValue("funnel")), &f)

	log("After unmarshal")
	log(f)

	le = len(f)
	if le == 0 {
		w.Write([]byte("empty funnel"))
		return
	}
	res = make([]int, le)

//	thirtydaysinseconds := int64(30 * 24 * 60 * 60)
//	endTimestamp := timestampInt() - thirtydaysinseconds

//	clean(analyticsId, f[0], endTimestamp)
	listId = formatTrackList(analyticsId, f[0])
	ll, _ = client.LLen(listId)
	if d1 != "" && d2 != "" {
		start, end = getPointers(listId, d1, d2)
	} else {
		start = 0
		end = int(ll)
	}
	for i = start; i < end; i++ {
		bdata, _ = client.LIndex(listId, i)
		data = string(bdata)
		tmp = strings.Split(data, ":")
		cookieId, stimestamp = tmp[0], tmp[1]
		timestamp, _ = strconv.ParseInt(stimestamp, 10, 64)
		if _, ok = M[cookieId]; !ok {
			M[cookieId] = timestamp
		}
	}
	res[0] = len(M)

	for i = 1; i < le; i++ {
		listId = formatTrackList(analyticsId, f[i])
//		clean(analyticsId, f[i], endTimestamp)
		ll, _ = client.LLen(listId)
		if d1 != "" && d2 != "" {
			start, end = getPointers(listId, d1, d2)
		} else {
			start = 0
			end = int(ll)
		}
		M2 = make(map[string]int64)
		for j = start; j < end; j++ {
			bdata, _ = client.LIndex(listId, j)
			data = string(bdata)
			tmp = strings.Split(data, ":")
			cookieId, stimestamp = tmp[0], tmp[1]
			timestamp, _ = strconv.ParseInt(stimestamp, 10, 64)
			if val, ok = M[cookieId]; ok {
				if val < timestamp {
					if _, ok2 = M2[cookieId]; !ok2 {
						M2[cookieId] = timestamp
					}
				}
			}
		}
		M = M2
		res[i] = len(M)
	}

	log("Res: ", res)

	response, _ := json.Marshal(res)
	w.Write(response)
}

func getItemFromList(li string, index int) int64 {
	bulk, _ := client.LIndex(li, index)
//	log(bulk)
	if bulk == nil {
		return -1
	}
	sbulk := string(bulk)
	tmp, _ := strconv.ParseInt(strings.Split(sbulk, ":")[1], 10, 64)
	return tmp
}

func bound(li string, timestamp string, lower bool) int {
	var l, r, mid int
	var itimestamp, tmp, tr int64

	itimestamp, _ = strconv.ParseInt(timestamp, 10, 64)

	l = 0
	tr, _ = client.LLen(li)
	r = int(tr)
	mid = -1

	tmp = getItemFromList(li, 0)
	if tmp == -1 || tmp > itimestamp {
		return 0
	}

	tmp = getItemFromList(li, r-1)
	if tmp < itimestamp {
		return r
	}

	for l < r {
		mid = (r + l) / 2
		log("l: ", l, "r: ", r, "mid: ", mid)

		tmp = getItemFromList(li, mid)
		//		log(itimestamp, " ", tmp)

		if itimestamp > tmp {
			if l == mid {
				break
			}
			l = mid
		} else if itimestamp == tmp && !lower {
			if l == mid {
				break
			}
			l = mid
		} else {
			if r == mid + 1 {
				break
			}
			r = mid + 1
		}
		//		log("new l: ", l, "new r: ", r)
	}

	tmp = getItemFromList(li, mid)
	if tmp < itimestamp {
		return mid + 1
	}

	return mid
}

func getPointers(li string, d1 string, d2 string) (int, int) {
	var p1, p2 int

	p1 = bound(li, d1, true)
	p2 = bound(li, d2, false)
	log("    --> lower bound (p1): ", p1)
	log("    --> upper bound (p2): ", p2)

	return p1, p2
}

func statisticsHandler(w http.ResponseWriter, r *http.Request) {
	log("\n-- statistics: ", r.URL.Path, " ; args: ", r.URL.Query())
	statistics, ok := r.URL.Query()["analyticsId"]

	if !ok {
		log(" --> drop request; no \"analyticsId\"")
		return
	}

	analyticsId := statistics[0]
	pages, _ := client.SMembers(formatPages(analyticsId))
	type JSONStatisticsResponse struct {
		UniqueVisits   map[string]string
		TotalVisits    map[string]string
		TotalTimeSpent string
		TotalHits      string
	}

	resp := make(map[string]JSONStatisticsResponse)
	for _, p := range pages {
		var tmp, _ = client.HGetAll(formatPage(analyticsId, p))
		var times = uniquePeriodsOfPage(analyticsId, p)
		var rmap = make(map[string]string)

		for _, val := range times {
			tval, _ := client.SCard(formatUniquePageLoads(analyticsId, p, val))
			rmap[val] = fmt.Sprintf("%d", tval)
		}

		val1, _ := client.Get(formatAverageTimePageHits(analyticsId, p))
		val2, _ := client.Get(formatAverageTimePageTime(analyticsId, p))
		resp[p] = JSONStatisticsResponse{
			TotalVisits:    tmp,
			UniqueVisits:   rmap,
			TotalHits:      string(val1),
			TotalTimeSpent: string(val2),
		}
	}

	message, _ := json.MarshalIndent(resp, "", " ")
	w.Header().Set("Content-type", "application/json")
	w.Write(message)
}

func formatAverageTimePageSession(analyticsId, page, cookieId string) string {
	return fmt.Sprintf("%s:av_time:%s:%s", analyticsId, hashPage(page), cookieId)
}

func timestampInt() int64 {
	return time.Now().UTC().Unix()
}

func timestampStr() string {
	return fmt.Sprintf("%d", timestampInt())
}

func keepAliveHandler(w http.ResponseWriter, r *http.Request) {
	var (
		analyticsId string
		page        string
		timestamp   string
		ok          bool
	)

	log("\n-- keepAlive: ", r.URL.Path, " ; args: ", r.URL.Query())

	if analyticsId, ok = checkRequest(r); !ok {
		return
	}

	page = getArg(r, "page")
	if page == "" {
		log("   -->  drop request; no \"page\" argument in keepAlive")
		return
	}

	timestamp = getArg(r, "lastActivity")
	if timestamp == "" {
		log("   -->  drop request; no \"lastActivity\" argument in keepAlive")
		return
	}

	lastActivity, err := strconv.ParseInt(timestamp, 10, 64)
	if err != nil || lastActivity > timestampInt() {
		log("  --> drop request; invalid lastActivity value")
		return
	}

	if cookie, err := r.Cookie("init"); err == nil {
		cookieId := cookie.Value
		formatStr := formatAverageTimePageSession(analyticsId, page, cookieId)
		addVisitors(analyticsId, cookieId)

		client.Expire(formatStr, 1800)
	}

	returnImage(&w)
}

func formatAverageTimePageHits(analyticsId, page string) string {
	return analyticsId + ":reqhits:" + page
}

func formatAverageTimePageTime(analyticsId, page string) string {
	return analyticsId + ":reqtime:" + page
}

func finishHandler(w http.ResponseWriter, r *http.Request) {
	var (
		analyticsId string
		page        string
		result      []byte
		ok          bool
		gerr        error
	)

	log("\n-- finish: ", r.URL.Path, " ; args: ", r.URL.Query())

	if analyticsId, ok = checkRequest(r); !ok {
		return
	}

	page = getArg(r, "page")
	if page == "" {
		log("   -->  drop request; no \"page\" argument in keepAlive")
		return
	}
	if checkDropRequest(analyticsId, r.URL.RawQuery) {
		return
	}

	if cookie, err := r.Cookie("init"); err == nil {
		cookieId := cookie.Value
		formatStr := formatAverageTimePageSession(analyticsId, page, cookieId)

		client.SRem(formatVisitors(analyticsId), cookieId)
		client.Del(formatVisitor(analyticsId, cookieId))

		result, _ = client.Get(formatStr)

		if result == nil {
			log(" -- drop request; no 'init' time ")
			return
		}

		t1, _ := strconv.ParseInt(string(result), 10, 64)
		t2 := timestampInt()
		if gerr != nil {
			log("  --> Error within finish: ", err)
			return
		}

		fmt.Print("  --> page: ", page, " ; time: ", t2-t1)
		client.Incr(formatAverageTimePageHits(analyticsId, page))
		client.IncrBy(formatAverageTimePageTime(analyticsId, page), int(t2-t1))
	}

	returnImage(&w)
}

func uniquePeriodsOfPage(analyticsId, page string) []string {
	var res, _ = client.SMembers(formatUniquePageLoadsTimes(analyticsId, page))
	return res
}

func initServer() {
	var err error
	rand.Seed(time.Now().UTC().UnixNano())

	client, err = goredis.Dial(&goredis.DialConfig{protocol, host, db, password, timeout, maxidle})

	if err != nil {
		log(" -- error with redis: ", err)
		panic(err)
	}

	client.Set(persistentKey, "1", 0, 0, false, false)
	go func() {
		for {
			time.Sleep(5 * time.Second)
			//			log("*** check client")
			data, _ := client.Get(persistentKey)
			if data == nil {
				//				log("*** initializing client")
				client, err = goredis.Dial(&goredis.DialConfig{protocol, host, db, password, timeout, maxidle})
				data, _ = client.Get(persistentKey)
				log(data)
			}
		}
	}()

	go func() {
		http.HandleFunc("/send_data/", trackHandler)
		http.HandleFunc("/init/", initHandler)
		http.HandleFunc("/statistics/", statisticsHandler)
		http.HandleFunc("/users/", usersHandler)
//		http.HandleFunc("/clean/", cleanHandler)
		http.HandleFunc("/keepAlive/", keepAliveHandler)
		http.HandleFunc("/finish/", finishHandler)
		http.HandleFunc("/funnel/", funnelHandler)
		http.ListenAndServe(":8000", nil)
	}()
}

func main() {
	var a string

	log("[X] Start BasicStats server")
	log("Press enter to stop the server")

	initServer()

	fmt.Scanf("%s\n", &a)
	log("[X] End BasicStats server")
}
