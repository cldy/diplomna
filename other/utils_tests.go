package main

import (
	"fmt"
	"strconv"
	"strings"
)

func find_bound(timestamp string, arr []string, lower bool) int {
	var l, r, mid int
	var itimestamp, tmp int64

	itimestamp, _ = strconv.ParseInt(timestamp, 10, 64)

	l = 0
	r = len(arr)
	mid = -1

	tmp, _ = strconv.ParseInt(strings.Split(arr[0], ":")[1], 10, 64)
	if tmp > itimestamp {
		return 0
	}

	tmp, _ = strconv.ParseInt(strings.Split(arr[r-1], ":")[1], 10, 64)
	if tmp < itimestamp {
		return r
	}

	for l < r {
		mid = (r + l) / 2
//		fmt.Println("l: ", l, "r: ", r, "mid: ", mid)

		tmp, _ = strconv.ParseInt(strings.Split(arr[mid], ":")[1], 10, 64)
//		fmt.Println(itimestamp, " ", tmp)

		if itimestamp > tmp {
			if l == mid {
				break
			}
			l = mid
		} else if itimestamp == tmp && !lower {
			if l == mid {
				break
			}
			l = mid
		} else {
			if r == mid + 1 {
				break
			}
			r = mid + 1
		}
//		fmt.Println("new l: ", l, "new r: ", r)
	}

	tmp, _ = strconv.ParseInt(strings.Split(arr[mid], ":")[1], 10, 64)
	if tmp < itimestamp {
		return mid + 1
	}

	return mid
}

func main() {
	var n int
	var data [1000]string
	var searched string

	fmt.Scanf("%d\n", &n)
	for i := 0; i < n; i++ {
		fmt.Scanf("%s\n", &data[i])
	}
	fmt.Scanf("%s\n", &searched)
	fmt.Println(find_bound(searched, data[0:n], false))
}
