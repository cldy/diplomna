import os
import random
import string


def test_lower_bound(l):
    arr = sorted([random.randint(1, 1000) for i in range(l)])
    data = map(lambda x: ''.join(random.choice(string.ascii_lowercase) for z in range(random.randint(1, 3))) + ':' + str(x), arr)
    searched = random.choice(arr) + random.randint(-100, 100)


    index = None
    if arr[0] > searched:
        index = 0
    elif arr[-1] < searched:
        index = len(arr)
    else:
        for i, _ in enumerate(arr[1:], 1):
            if arr[i-1] <= searched <= arr[i]:
                index = i
                # break
    return data, searched, index

def main():
    data, s, ans = test_lower_bound(100)
    with open('test_file.in', 'w') as f:
        print >> f, len(data)
        for i in data:
            print >> f, i
        print >> f, s

    os.system('c:\\Go\\bin\\go run utils_tests.go < test_file.in > test_file.out')
    ans2 = int(open('test_file.out', 'r').read().strip())
    print ans == ans2

if __name__ == '__main__':
    main()