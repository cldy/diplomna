from django.conf.urls import patterns, include, url, static

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from website import settings

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'website.views.home', name='home'),
    # url(r'^website/', include('website.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    url(r'', include('home.urls')),
    url(r'^campaigns/', include('campaigns.urls')),
) + static.static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
