from django.contrib.auth.forms import UserCreationForm
from django import forms
from django.contrib.auth.models import User
from django.forms.models import ModelForm


class UserRegistrationFormWithEmail(UserCreationForm):
    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']

class UserChangePasswordForm(ModelForm):
    old_password = forms.CharField(label="Old password",
                               widget=forms.PasswordInput)
    password1 = forms.CharField(label="New password",
                                widget=forms.PasswordInput)
    password2 = forms.CharField(label="New password confirmation",
                            widget=forms.PasswordInput,
                            help_text="Enter the same password as above, for verification.")

    class Meta:
        fields = ['old_password', 'password1', 'password2']
        model = User

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', '')
        super(UserChangePasswordForm, self).__init__(*args, **kwargs)

    def clean_old_password(self):
        real_user = self.user
        old_password = self.cleaned_data.get('old_password')
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if password1 and password2 and password1 != password2:
            raise forms.ValidationError('Passwords does not match')

        if real_user.check_password(old_password):
            return password1

        raise forms.ValidationError('Old password is not correct')

    def save(self, commit=True):
        password = self.cleaned_data['password1']
        self.user.set_password(password)
        if commit:
            self.user.save()
        return self.user