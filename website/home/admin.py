from django.contrib.sites.models import Site
from django.contrib.auth.models import Group

from django.contrib import admin
from campaigns.models import Funnel, Campaign

admin.site.register(Funnel)
admin.site.register(Campaign)
admin.site.unregister(Group)
admin.site.unregister(Site)