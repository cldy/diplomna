# Create your views here.
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login as dlogin, logout as dlogout

from home.forms import UserRegistrationFormWithEmail, UserChangePasswordForm


def index(request):
    if request.user.is_authenticated():
        return redirect('campaigns.views.index')
    return render(request, 'home/index.html')


def login(request):
    error = None

    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)

        if user is not None:
            dlogin(request, user)
            return redirect('campaigns.views.index')

        error = 'Invalid username/password'

    return render(request, 'home/index.html', {
        'error': error
    })


def logout(request):
    dlogout(request)
    return redirect('home.views.index')


def register(request):
    if request.method == 'POST':
        form = UserRegistrationFormWithEmail(request.POST)
        if form.is_valid():
            form.save()
            return redirect('home.views.index')
    else:
        form = UserRegistrationFormWithEmail()
    return render(request, 'home/register.html', {'form': form})


@login_required
def change_password(request):
    if request.method == 'POST':
        form = UserChangePasswordForm(request.POST, user=request.user)
        if form.is_valid():
            form.save()
            return redirect('home.views.index')
    else:
        form = UserChangePasswordForm(user=request.user)

    return render(request, 'home/change_password.html', {'form': form})