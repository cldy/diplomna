from django.conf.urls import patterns, url

urlpatterns = patterns('',
    url(r'^$', 'home.views.index'),
    url(r'login/', 'home.views.login'),
    url(r'logout/', 'home.views.logout'),
    url(r'register/', 'home.views.register'),
    url(r'change_password/', 'home.views.change_password'),
)
