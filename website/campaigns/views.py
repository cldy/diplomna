import datetime
import json

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http.response import HttpResponse, HttpResponseForbidden, HttpResponseNotFound
from django.shortcuts import render
from django.views.decorators.http import require_POST
import time

from campaigns.models import Campaign, Funnel
from utils.general_utils import hash_page


@login_required
def index(request):
    campaigns = Campaign.objects.filter(owners__id=request.user.id)
    return render(request, 'campaigns/index.html', {
        'campaigns': campaigns,
    })


@login_required
def create_campaign(request):
    name = request.POST.get('name')

    status, data = Campaign.create_campaign(name, request.user)
    if not status:
        return HttpResponse(json.dumps({
            'success': False,
            'message': data,
        }), content_type='application/json')

    return HttpResponse(json.dumps({
        'success': True,
        'id': data.id
    }), content_type='application/json')


@login_required
def statistics(request, campaign_id):
    try:
        campaign = Campaign.objects.get(id=campaign_id)
        if not campaign.can_edit(request.user):
            return HttpResponseForbidden('You are not the owner of the campaign')
    except:
        return HttpResponseNotFound('No such campaign')

    try:
        stats = campaign.get_statistics()
        json_data = json.loads(stats)
        page_data = []
        for page in json_data:
            try:
                hits = int(json_data[page]['TotalHits'])
            except:
                continue

            page_data.append([page, hits, hash_page(page)])

        pages = sorted(page_data, key=lambda x: -x[1])
        funnels = campaign.funnel_set.all()
    except ValueError as e:
        pages = []
        message = unicode(e)
        print message

    return render(request, 'campaigns/campaign.html', {
        'campaign': campaign,
        'statistics': campaign.get_statistics(),
        'pages': pages,
        'funnels': funnels,
    })


@login_required
@require_POST
def remove_campaign(request, campaign_id):
    try:
        fail = HttpResponse(json.dumps({
            'success': False,
        }), content_type='application/json')

        campaign = Campaign.objects.get(id=campaign_id)
        if not campaign.can_edit(request.user):
            return fail
    except:
        return fail

    campaign.delete()
    return HttpResponse(json.dumps({
        'success': True,
    }), content_type='application/json')


def get_stats(request, campaign_id, hashed_page):
    try:
        campaign = Campaign.objects.get(id=campaign_id)
        if not campaign.can_edit(request.user):
            return HttpResponseForbidden('You are not the owner of the campaign')
    except:
        return HttpResponseNotFound('No such campaign')

    try:
        statistics = json.loads(campaign.get_statistics())
    except:
        return HttpResponse('There was a problem with the statistics. Please try again later.')

    for page in statistics:
        if hash_page(page) == hashed_page:
            time_per_page = 0
            hits = float(statistics[page].get('TotalHits', 0))
            if hits != 0:
                time_per_page = str(datetime.timedelta(seconds=float(statistics[page].get('TotalTimeSpent')) / hits))
                time_per_page = time_per_page.split('.')[0]

            all_time = str(datetime.timedelta(seconds=int(statistics[page].get('TotalTimeSpent', 0))))
            all_time = all_time.split('.')[0]

            return render(request, 'campaigns/page.html', {
                'page': page,
                'data': json.dumps(statistics[page]),
                'time_per_page': time_per_page,
                'all_time': all_time,
                'hits': statistics[page].get('TotalHits', 0),
            })

    return HttpResponse('Page not found')


@login_required
@require_POST
def save_funnel(request, funnel_id):
    try:
        funnel = Funnel.objects.get(id=funnel_id)
        if not funnel.campaign.can_edit(request.user):
            return HttpResponseForbidden('Not the owner')
    except Exception as e:
        return HttpResponse(json.dumps({
            'success': False,
            'message': unicode(e),
        }), content_type='application/json')

    new_data = request.POST.get('data')
    status, message = funnel.update_events(new_data)

    return HttpResponse(json.dumps({
        'success': status,
        'message': message,
    }), content_type='application/json')


@login_required
@require_POST
def remove_funnel(request, funnel_id):
    try:
        funnel = Funnel.objects.get(id=funnel_id)
        if not funnel.campaign.can_edit(request.user):
            return HttpResponseForbidden('Not the owner')
    except Exception as e:
        return HttpResponse(json.dumps({
            'success': False,
            'message': unicode(e),
        }), content_type='application/json')

    funnel.delete()
    return HttpResponse(json.dumps({
        'success': True,
    }), content_type='application/json')


def create_funnel(request, campaign_id):
    try:
        campaign = Campaign.objects.get(id=campaign_id)
        if not campaign.can_edit(request.user):
            return HttpResponseForbidden('Not the owner')
    except Exception as e:
        return HttpResponse(json.dumps({
            'success': False,
            'message': unicode(e),
        }), content_type='application/json')

    f = Funnel(campaign_id=campaign_id, name=request.POST.get('name'), events="")
    f.save()

    return HttpResponse(json.dumps({
        'success': True,
        'id': f.id,
        'name': f.name,
    }), content_type='application/json')


@login_required
def query_funnel(request, funnel_id):
    try:
        funnel = Funnel.objects.get(id=funnel_id)
        if not funnel.campaign.can_edit(request.user):
            return HttpResponseForbidden('Not the owner')
    except Exception as e:
        return HttpResponse(json.dumps({
            'success': False,
            'message': unicode(e),
        }), content_type='application/json')

    return HttpResponse(funnel.query(request.GET.get('d1'), request.GET.get('d2')), content_type='application/json')


@login_required
def add_users(request, campaign_id):
    campaign = Campaign.objects.get(id=campaign_id)
    if not campaign.can_edit(request.user):
        return HttpResponseForbidden()

    users = User.objects.all().exclude(id__in=campaign.owners.values('id'))
    added_users = campaign.owners.all()
    return render(request, 'campaigns/add_users.html', {
        'invite_users': users,
        'added_users': added_users,
        'campaign': campaign,
        'fu_django_templates': len(users),
    })


@login_required
def invite_user(request, campaign_id, user_id):
    try:
        campaign = Campaign.objects.get(id=campaign_id)
        if not campaign.can_edit(request.user):
            return HttpResponseForbidden()
        user = User.objects.get(id=user_id)
    except:
        return HttpResponseNotFound()

    campaign.owners.add(user)

    return HttpResponse(json.dumps({
        'success': True,
    }), content_type='application/json')


@login_required
def remove_user(request, campaign_id, user_id):
    try:
        campaign = Campaign.objects.get(id=campaign_id)
        if not campaign.can_edit(request.user):
            return HttpResponseForbidden()
        user = User.objects.get(id=user_id)
    except:
        return HttpResponseNotFound()

    campaign.owners.remove(user)

    return HttpResponse(json.dumps({
        'success': True,
    }), content_type='application/json')


@login_required
def live_users(request, campaign_id):
    TIMEOUT_SECS = 20
    last = request.GET.get('last')

    try:
        campaign = Campaign.objects.get(id=campaign_id)
    except:
        return HttpResponseNotFound()
    if not campaign.can_edit(request.user):
        return HttpResponseForbidden()
    try:
        for _ in range(TIMEOUT_SECS):
            time.sleep(1)
            users = campaign.get_users_live()
            if users != last:
                break
    except Exception as e:
        return HttpResponse(json.dumps({
            'success': False,
            'message': unicode(e),
        }), content_type='application/json')

    return HttpResponse(json.dumps({
        'success': True,
        'users': users,
    }), content_type='application/json')