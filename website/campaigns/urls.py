from django.conf.urls import patterns, url

urlpatterns = patterns('',
    url(r'^$', 'campaigns.views.index'),
    url(r'^create_campaign$', 'campaigns.views.create_campaign'),
    url(r'^statistics/(\d+)/$', 'campaigns.views.statistics'),
    url(r'^remove_campaign/(\d+)/$', 'campaigns.views.remove_campaign'),
    url(r'^get_stats/(\d+)/(\w+)/$', 'campaigns.views.get_stats'),
    url(r'^save_funnel/(\d+)/$', 'campaigns.views.save_funnel'),
    url(r'^remove_funnel/(\d+)/$', 'campaigns.views.remove_funnel'),
    url(r'^create_funnel/(\d+)/$', 'campaigns.views.create_funnel'),
    url(r'^query_funnel/(\d+)/$', 'campaigns.views.query_funnel'),
    url(r'^add_users/(\d+)/$', 'campaigns.views.add_users'),
    url(r'^invite_user/(\d+)/(\d+)$', 'campaigns.views.invite_user'),
    url(r'^remove_user/(\d+)/(\d+)$', 'campaigns.views.remove_user'),
    url(r'^live_users/(\d+)/$', 'campaigns.views.live_users'),
)
