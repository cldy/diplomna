import json
import random
import re

from django.core.exceptions import ValidationError
from django.core.validators import MinLengthValidator, MaxLengthValidator
from django.db import models
from django.contrib.auth.models import User
import requests
from requests.exceptions import ConnectionError

from website import settings


def generate_identifier(length=16):
    return ''.join([random.choice('0123456789') for _ in range(length)])


class Campaign(models.Model):
    identifier = models.CharField(max_length=16)
    name = models.CharField(max_length=254, validators=[MinLengthValidator(3), MaxLengthValidator(32)])
    owners = models.ManyToManyField(User)

    @classmethod
    def create_campaign(cls, name, owner):
        c = Campaign(name=name, identifier=generate_identifier())
        try:
            c.full_clean()
        except ValidationError as e:
            message = ''
            for val, l in e.message_dict.iteritems():
                message = str(val) + ': ' + ';'.join(l) + '\n'
            return False, message
        c.save()
        c.owners.add(owner)
        return True, c

    def get_statistics(self):
        try:
            json_resp = requests.get(settings.SERVER_URL + 'statistics/', params={'analyticsId': self.identifier})
        except ConnectionError as e:
            return 'Connection problems ({0}); try again later'.format(unicode(e))
        return json_resp.text

    def can_edit(self, user):
        return user in self.owners.all()

    def get_users_live(self):
        try:
            json_resp = requests.get(settings.SERVER_URL + 'users/', params={'analyticsId': self.identifier})
        except ConnectionError as e:
            return 'Connection problems ({0}); try again later'.format(unicode(e))
        return json_resp.text


class Funnel(models.Model):
    name = models.CharField(max_length=254)
    events = models.TextField()
    campaign = models.ForeignKey(Campaign)

    def update_events(self, new_data):
        if re.match('[a-zA-Z0-9,]+', new_data):
            self.events = new_data
            self.save()
            return True, None
        return False, 'Invalid data'

    def query(self, d1=None, d2=None):
        try:
            options = {'analyticsId': self.campaign.identifier}
            if d1 and d2:
                options['d1'] = d1
                options['d2'] = d2
            options['funnel'] = json.dumps(self.events.split(','))

            return json.dumps({
                'success': True,
                'data': requests.get(settings.SERVER_URL + 'funnel/', params=options).json(),
                'events': json.loads(options['funnel']),
            })
        except Exception as e:
            return json.dumps({
                'success': False,
                'message': unicode(e),
            })

