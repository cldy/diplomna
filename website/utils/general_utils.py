from hashlib import md5

def hash_page(page):
    m = md5(page)
    return m.hexdigest()